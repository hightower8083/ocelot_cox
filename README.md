# Module for <cite>[OCELOT]</cite> for transport simulations with the broad spectra (LPA) beams in the COXINEL-like beamlines.

### CONTAINS:
- beam generators for simple and sliced beams
- short-cut methods for:
  - beamline object creation
  - simulation running
  - sliced beam alignment
- beam diagnostics tool (for *make_shot* method))
- *slit* particle selection method
- tuning tool for super-matching of the beamline (object)
- printout of some S2I matricies coefficients
- beam export to <cite>[CHIMERA]</cite> for FEL, SR, PIC or SC simulations

## Attribution
Module is developed and supported by <cite>[Igor A Andriyash]</cite>

Special thanks to <cite>[Sergey Tomin]</cite> for advices and code pieces 

[OCELOT]: https://github.com/hightower8083/chimera
[CHIMERA]: https://github.com/iagapov/ocelot
[Igor A Andriyash]: mailto:igor.andriyash@gmail.com
[Sergey Tomin]: mailto:sergey.tomin@xfel.eu
